package com.jledesma.movierappi.data

import com.jledesma.movierappi.domain.Movie

val sampleMovie = Movie(
    616037,
    "Thor: Love and Thunder",
    "After his retirement is interrupted by Gorr the God Butcher, a galactic killer who seeks the extinction of the gods, Thor enlists the help of King Valkyrie, Korg, and ex-girlfriend Jane Foster, who now inexplicably wields Mjolnir as the Mighty Thor. Together they embark upon a harrowing cosmic adventure to uncover the mystery of the God Butcher’s vengeance and stop him before it’s too late.",
    "",
    "2022-07-06",
    "/pIkRyD18kl4FhoCNQuWxWu5cBLM.jpg",
    "en",
    "Thor: Love and Thunder",
    17290.383,
    6.7,
    false,
    "Popular"
)