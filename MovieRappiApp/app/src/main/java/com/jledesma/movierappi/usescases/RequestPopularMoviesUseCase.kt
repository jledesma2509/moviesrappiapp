package com.jledesma.movierappi.usescases

import com.jledesma.movierappi.data.MoviesRepository
import com.jledesma.movierappi.domain.Error
import javax.inject.Inject

class RequestPopularMoviesUseCase @Inject constructor(private val moviesRepository: MoviesRepository) {

    suspend operator fun invoke(): Error? {
        return moviesRepository.requestPopularMovies()
    }
}