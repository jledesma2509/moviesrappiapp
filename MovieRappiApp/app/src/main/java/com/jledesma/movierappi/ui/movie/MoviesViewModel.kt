package com.jledesma.movierappi.ui.movie

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jledesma.movierappi.domain.Movie
import com.jledesma.movierappi.usescases.GetPopularMoviesUseCase
import com.jledesma.movierappi.usescases.RequestPopularMoviesUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MoviesViewModel @Inject constructor(
    getPopularMoviesUseCase: GetPopularMoviesUseCase,
    private val requestPopularMoviesUseCase: RequestPopularMoviesUseCase
) : ViewModel() {

    private val _state = MutableStateFlow<MoviesState>(MoviesState.Init)
    val state: StateFlow<MoviesState> get() = _state.asStateFlow()

    init {
        viewModelScope.launch {
            getPopularMoviesUseCase()
                .catch {
                        cause -> _state.update { MoviesState.Error(cause.message.toString()) }
                }
                .collect {
                        movies -> _state.update { MoviesState.SuccessMovie(movies) }
                }

            }
    }

    fun onUiReady() {
        viewModelScope.launch {
            _state.value = MoviesState.IsLoading(true)
            requestPopularMoviesUseCase()
            _state.value = MoviesState.IsLoading(false)
        }
    }

    sealed class MoviesState {

        object Init : MoviesState()
        data class IsLoading(val isLoading: Boolean) : MoviesState()
        data class SuccessMovie(val movies: List<Movie>) : MoviesState()
        data class Error(val rawResponse: String) : MoviesState()

    }
}
