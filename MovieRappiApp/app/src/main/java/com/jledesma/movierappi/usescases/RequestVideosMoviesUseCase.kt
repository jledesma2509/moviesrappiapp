package com.jledesma.movierappi.usescases

import com.jledesma.movierappi.data.MoviesRepository
import com.jledesma.movierappi.domain.Error
import com.jledesma.movierappi.domain.Video
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class RequestVideosMoviesUseCase @Inject constructor(private val moviesRepository: MoviesRepository) {

    suspend operator fun invoke(id:Int): Flow<List<Video>> {
        return moviesRepository.requestVideosMovies(id)
    }
}