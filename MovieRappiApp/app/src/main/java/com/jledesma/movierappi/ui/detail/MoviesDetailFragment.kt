package com.jledesma.movierappi.ui.detail

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import coil.load
import com.google.android.youtube.player.YouTubeStandalonePlayer
import com.jledesma.movierappi.BuildConfig
import com.jledesma.movierappi.Constants
import com.jledesma.movierappi.domain.Video
import com.jledesma.movierappi.ui.common.BaseFragment
import com.jledesma.movierappi.ui.common.gone
import com.jledesma.movierappi.ui.common.showToast
import com.jledesma.movierappi.ui.common.visible
import kotlinx.coroutines.launch
import com.jledesma.movierappi.databinding.FragmentMoviesDetailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MoviesDetailFragment :  BaseFragment<FragmentMoviesDetailBinding>(FragmentMoviesDetailBinding::inflate) {

    private val safeArgs : MoviesDetailFragmentArgs by navArgs()

    private val viewModel: MoviesDetailViewModel by viewModels()

    private var idMovie = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.state.collect {
                    updateUI(it)
                }
            }
        }

    }

    private fun updateUI(state: MoviesDetailViewModel.MoviesVideoState) {
        when (state) {
            MoviesDetailViewModel.MoviesVideoState.Init -> Unit
            is MoviesDetailViewModel.MoviesVideoState.IsLoading -> handleLoading(state.isLoading)
            is MoviesDetailViewModel.MoviesVideoState.SuccessMoviesVideos -> handlerVideos(state.videos)
            is MoviesDetailViewModel.MoviesVideoState.Error -> requireActivity().showToast(state.rawResponse)


        }
    }

    private fun handlerVideos(videos: List<Video>) {
        if(videos.isNotEmpty()){
            startActivity(
                YouTubeStandalonePlayer.createVideoIntent(
                requireActivity(), BuildConfig.YOUTUBE_KEY,
                videos[0].key.toString(),
                1000,
                false,
                true))
        }else{

        }
    }

    private fun init() = with(binding) {
        safeArgs.movie.let { movie ->
            tvTitle.text = movie.title
            tvLanguage.text = movie.originalLanguage
            tvReleaseDate.text = movie.releaseDate.substring(0,4)
            tvAverage.text = movie.voteAverage.toString()
            tvOverview.text = movie.overview
            idMovie = movie.id

            binding.imgBackDrop.load("${Constants.URL_IMAGES}${movie.posterPath}") {
                //placeholder()
                crossfade(true)
                crossfade(400)
            }
        }

        imgBack.setOnClickListener {
            requireActivity().onBackPressed()
        }

        btnTrailer.setOnClickListener {
            viewModel.onClicked(idMovie)
        }
    }

    private fun handleLoading(isLoading: Boolean) {
        if (isLoading) binding.progress.visible()
        else binding.progress.gone()

    }


}