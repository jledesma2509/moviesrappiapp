package com.jledesma.movierappi.ui.movie

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import coil.transform.RoundedCornersTransformation
import com.jledesma.movierappi.Constants
import com.jledesma.movierappi.R
import com.jledesma.movierappi.databinding.FragmentMoviesBinding
import com.jledesma.movierappi.databinding.ItemRecommendedBinding
import com.jledesma.movierappi.databinding.ItemTrendingBinding
import com.jledesma.movierappi.databinding.ItemUpcomingBinding
import com.jledesma.movierappi.domain.Movie

import com.jledesma.movierappi.ui.common.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MoviesFragment : BaseFragment<FragmentMoviesBinding>(FragmentMoviesBinding::inflate) {

    private val viewModel: MoviesViewModel by viewModels()
    private var moviesFilter: List<Movie> = listOf()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.state.collect {
                    updateUI(it)
                }
            }
        }

        binding.viewLanguages.setOnClickListener {
            viewLanguagesActive()
            val moviesPopular = moviesFilter.filter {
                it.type == Constants.POPULAR && it.originalLanguage == Constants.FILTER_ORIGINAL_LANGUAGE
            }
            adapterRecommended.update(moviesPopular.take(6))
        }

        binding.viewReleases.setOnClickListener {
            viewReleasesActive()
            val moviesReleases = moviesFilter.filter {
                it.releaseDate.substring(0,4) == Constants.FILTER_YEAR
            }
            adapterRecommended.update(moviesReleases.take(6))

        }

        viewModel.onUiReady()

    }

    private fun updateUI(state: MoviesViewModel.MoviesState) {
        when (state) {
            MoviesViewModel.MoviesState.Init -> Unit
            is MoviesViewModel.MoviesState.IsLoading -> handleLoading(state.isLoading)
            is MoviesViewModel.MoviesState.SuccessMovie -> handlerMovies(state.movies)
            is MoviesViewModel.MoviesState.Error -> requireActivity().showToast(state.rawResponse)


        }
    }

    private fun handleLoading(isLoading: Boolean) {
        if (isLoading) binding.progress.visible()
        else binding.progress.gone()

    }

    private fun setupRecyclerView() = with(binding) {
        rvRecommended.adapter = adapterRecommended
        rvTrend.layoutManager = LinearLayoutManager(requireContext(),LinearLayoutManager.HORIZONTAL,false)
        rvTrend.adapter = adapterTrending
        rvUpcoming.layoutManager = LinearLayoutManager(requireContext(),LinearLayoutManager.HORIZONTAL,false)
        rvUpcoming.adapter = adapterUpcoming
    }

    private fun handlerMovies(movies: List<Movie>) {
        val moviesPopular = movies.filter {
            it.type == Constants.POPULAR && it.originalLanguage == Constants.FILTER_ORIGINAL_LANGUAGE
        }
        val moviesTrending = movies.filter {
            it.type == Constants.TRENDING
        }
        val moviesUpcoming = movies.filter {
            it.type == Constants.UPCOMING
        }
        adapterRecommended.update(moviesPopular.take(6))
        adapterTrending.update(moviesTrending)
        adapterUpcoming.update(moviesUpcoming)
        moviesFilter = movies
    }

    private fun viewLanguagesActive(){
        binding.viewLanguages.setBackgroundResource(R.drawable.view_recommended_active)
        binding.tvLanguageText.setTextColor(Color.BLACK)

        binding.viewReleases.setBackgroundResource(R.drawable.view_recommended_inactive)
        binding.tvReleaseText.setTextColor(Color.WHITE)
    }

    private fun viewReleasesActive(){
        binding.viewLanguages.setBackgroundResource(R.drawable.view_recommended_inactive)
        binding.tvLanguageText.setTextColor(Color.WHITE)

        binding.viewReleases.setBackgroundResource(R.drawable.view_recommended_active)
        binding.tvReleaseText.setTextColor(Color.BLACK)
    }

    var adapterRecommended: BaseAdapter<Movie> = object : BaseAdapter<Movie>(emptyList()) {
        override fun getViewHolder(parent: ViewGroup): BaseViewHolder<Movie> {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_recommended, parent, false)
            return object : BaseAdapter.BaseViewHolder<Movie>(view) {
                private val binding: ItemRecommendedBinding = ItemRecommendedBinding.bind(view)
                override fun bind(entity: Movie)  {

                    binding.imgRecommended.load("${Constants.URL_IMAGES}${entity.posterPath}") {
                        //placeholder()
                        crossfade(true)
                        crossfade(400)
                        transformations(RoundedCornersTransformation(50f))
                    }


                    binding.root.setOnClickListener {
                        navigateToDetail(entity)
                    }

                }
            }
        }
    }

    var adapterTrending: BaseAdapter<Movie> = object : BaseAdapter<Movie>(emptyList()) {
        override fun getViewHolder(parent: ViewGroup): BaseViewHolder<Movie> {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_trending, parent, false)
            return object : BaseAdapter.BaseViewHolder<Movie>(view) {
                private val binding: ItemTrendingBinding = ItemTrendingBinding.bind(view)
                override fun bind(entity: Movie)  {

                    binding.imgTrending.load("${Constants.URL_IMAGES}${entity.posterPath}") {
                        //placeholder()
                        crossfade(true)
                        crossfade(400)
                        transformations(RoundedCornersTransformation(50f))
                    }

                    binding.root.setOnClickListener {
                        navigateToDetail(entity)
                    }

                }
            }
        }
    }

    var adapterUpcoming: BaseAdapter<Movie> = object : BaseAdapter<Movie>(emptyList()) {
        override fun getViewHolder(parent: ViewGroup): BaseViewHolder<Movie> {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_upcoming, parent, false)
            return object : BaseAdapter.BaseViewHolder<Movie>(view) {
                private val binding: ItemUpcomingBinding = ItemUpcomingBinding.bind(view)
                override fun bind(entity: Movie)  {

                    binding.imgUpcoming.load("${Constants.URL_IMAGES}${entity.posterPath}") {
                        //placeholder()
                        crossfade(true)
                        crossfade(400)
                        transformations(RoundedCornersTransformation(50f))
                    }

                    binding.root.setOnClickListener {
                        navigateToDetail(entity)
                    }

                }
            }
        }
    }

    private fun navigateToDetail(movie:Movie){
        val directions = MoviesFragmentDirections.actionMoviesFragmentToMoviesDetailFragment(movie!!)
        navigateToDirections(directions)
    }


}