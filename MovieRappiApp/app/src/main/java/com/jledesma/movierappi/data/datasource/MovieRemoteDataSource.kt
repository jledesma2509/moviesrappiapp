package com.jledesma.movierappi.data.datasource

import com.jledesma.movierappi.domain.RemoteResult
import com.jledesma.movierappi.domain.RemoteVideoResult
import com.jledesma.movierappi.domain.Video
import kotlinx.coroutines.flow.Flow


interface MovieRemoteDataSource {

    suspend fun findPopularMovies() : RemoteResult

    suspend fun findTrendingMovies() :  RemoteResult

    suspend fun findUpcomingMovies() : RemoteResult

    suspend fun findVideosMovies(id:Int) : RemoteVideoResult
}