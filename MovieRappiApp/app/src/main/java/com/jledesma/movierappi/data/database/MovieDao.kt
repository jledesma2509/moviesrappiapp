package com.jledesma.movierappi.data.database

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface MovieDao {

    @Query("SELECT *FROM DbMovie")
    fun getAll(): Flow<List<DbMovie>>

    @Query("SELECT *FROM DbMovie WHERE id =:id")
    fun findById(id:Int):Flow<DbMovie>

    @Query("SELECT COUNT(id) FROM DbMovie where type=:type")
    suspend fun movieCount(type:String):Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovies(dbMovies:List<DbMovie>)

    @Update
    suspend fun updateMovie(movies: DbMovie)
}