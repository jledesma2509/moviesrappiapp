package com.jledesma.movierappi.data.server

import com.jledesma.movierappi.domain.RemoteResult
import com.jledesma.movierappi.domain.RemoteVideoResult
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RemoteService {

    @GET("movie/popular")
    suspend fun listPopularMovies(
        @Query("api_key") apiKey: String,
    ): RemoteResult

    @GET("trending/movie/day")
    suspend fun listTrendingMovies(
        @Query("api_key") apiKey: String,
    ): RemoteResult

    @GET("movie/upcoming")
    suspend fun listUpcomingMovies(
        @Query("api_key") apiKey: String,
    ): RemoteResult

    @GET("movie/{id}/videos")
    suspend fun listVideosMovies(
        @Path("id") id:Int,
        @Query("api_key") apiKey: String,
    ): RemoteVideoResult

}