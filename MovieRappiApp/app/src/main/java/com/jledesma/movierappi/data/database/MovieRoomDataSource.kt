package com.jledesma.movierappi.data.database


import com.jledesma.movierappi.data.datasource.MovieLocalDataSource
import com.jledesma.movierappi.domain.Error
import com.jledesma.movierappi.domain.Movie
import com.jledesma.movierappi.domain.tryCall
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class MovieRoomDataSource @Inject constructor(private val movieDao: MovieDao) :
    MovieLocalDataSource {

    override val movies: Flow<List<Movie>> = movieDao.getAll().map { it.toDomainModel() }

    override suspend fun isEmptyPopular(): Boolean = movieDao.movieCount("Popular") == 0


    override suspend fun isEmptyTrending(): Boolean = movieDao.movieCount("Trending") == 0

    override suspend fun isUpcomingTrending(): Boolean =  movieDao.movieCount("Upcoming") == 0

    override suspend fun save(movies: List<DbMovie>): Error? = tryCall {
        movieDao.insertMovies(movies)
    }

}

private fun List<DbMovie>.toDomainModel(): List<Movie> = map { it.toDomainModel() }

private fun DbMovie.toDomainModel(): Movie =
    Movie(
        id,
        title,
        overview,
        releaseDate,
        posterPath,
        backdropPath,
        originalLanguage,
        originalTitle,
        popularity,
        voteAverage,
        favorite,
        type
    )

