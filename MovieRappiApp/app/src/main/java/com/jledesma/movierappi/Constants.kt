package com.jledesma.movierappi

class Constants {

    companion object{

        const val FILTER_ORIGINAL_LANGUAGE = "en"
        const val FILTER_YEAR = "2021"

        const val POPULAR = "Popular"
        const val TRENDING = "Trending"
        const val UPCOMING = "Upcoming"

        const val URL_IMAGES = "https://image.tmdb.org/t/p/w500/"

    }
}