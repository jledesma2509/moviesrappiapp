package com.jledesma.movierappi.domain;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bv\u0018\u00002\u00020\u0001:\u0003\u0002\u0003\u0004\u0082\u0001\u0003\u0005\u0006\u0007\u00a8\u0006\b"}, d2 = {"Lcom/jledesma/movierappi/domain/Error;", "", "Connectivity", "Server", "Unknown", "Lcom/jledesma/movierappi/domain/Error$Server;", "Lcom/jledesma/movierappi/domain/Error$Connectivity;", "Lcom/jledesma/movierappi/domain/Error$Unknown;", "app_debug"})
public abstract interface Error {
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/jledesma/movierappi/domain/Error$Server;", "Lcom/jledesma/movierappi/domain/Error;", "code", "", "(I)V", "getCode", "()I", "app_debug"})
    public static final class Server implements com.jledesma.movierappi.domain.Error {
        private final int code = 0;
        
        public Server(int code) {
            super();
        }
        
        public final int getCode() {
            return 0;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/jledesma/movierappi/domain/Error$Connectivity;", "Lcom/jledesma/movierappi/domain/Error;", "()V", "app_debug"})
    public static final class Connectivity implements com.jledesma.movierappi.domain.Error {
        @org.jetbrains.annotations.NotNull()
        public static final com.jledesma.movierappi.domain.Error.Connectivity INSTANCE = null;
        
        private Connectivity() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/jledesma/movierappi/domain/Error$Unknown;", "Lcom/jledesma/movierappi/domain/Error;", "message", "", "(Ljava/lang/String;)V", "getMessage", "()Ljava/lang/String;", "app_debug"})
    public static final class Unknown implements com.jledesma.movierappi.domain.Error {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String message = null;
        
        public Unknown(@org.jetbrains.annotations.NotNull()
        java.lang.String message) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getMessage() {
            return null;
        }
    }
}