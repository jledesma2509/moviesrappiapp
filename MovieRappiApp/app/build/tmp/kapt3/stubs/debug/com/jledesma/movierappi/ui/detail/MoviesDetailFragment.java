package com.jledesma.movierappi.ui.detail;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0016\u0010\u0016\u001a\u00020\u00132\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00190\u0018H\u0002J\b\u0010\u001a\u001a\u00020\u0013H\u0002J\u001a\u0010\u001b\u001a\u00020\u00132\u0006\u0010\u001c\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016J\u0010\u0010 \u001a\u00020\u00132\u0006\u0010!\u001a\u00020\"H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0006\u001a\u00020\u00078BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\b\u0010\tR\u001b\u0010\f\u001a\u00020\r8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u000e\u0010\u000f\u00a8\u0006#"}, d2 = {"Lcom/jledesma/movierappi/ui/detail/MoviesDetailFragment;", "Lcom/jledesma/movierappi/ui/common/BaseFragment;", "Lcom/jledesma/movierappi/databinding/FragmentMoviesDetailBinding;", "()V", "idMovie", "", "safeArgs", "Lcom/jledesma/movierappi/ui/detail/MoviesDetailFragmentArgs;", "getSafeArgs", "()Lcom/jledesma/movierappi/ui/detail/MoviesDetailFragmentArgs;", "safeArgs$delegate", "Landroidx/navigation/NavArgsLazy;", "viewModel", "Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel;", "getViewModel", "()Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "handleLoading", "", "isLoading", "", "handlerVideos", "videos", "", "Lcom/jledesma/movierappi/domain/Video;", "init", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "updateUI", "state", "Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel$MoviesVideoState;", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class MoviesDetailFragment extends com.jledesma.movierappi.ui.common.BaseFragment<com.jledesma.movierappi.databinding.FragmentMoviesDetailBinding> {
    private final androidx.navigation.NavArgsLazy safeArgs$delegate = null;
    private final kotlin.Lazy viewModel$delegate = null;
    private int idMovie = 0;
    
    public MoviesDetailFragment() {
        super(null);
    }
    
    private final com.jledesma.movierappi.ui.detail.MoviesDetailFragmentArgs getSafeArgs() {
        return null;
    }
    
    private final com.jledesma.movierappi.ui.detail.MoviesDetailViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void updateUI(com.jledesma.movierappi.ui.detail.MoviesDetailViewModel.MoviesVideoState state) {
    }
    
    private final void handlerVideos(java.util.List<com.jledesma.movierappi.domain.Video> videos) {
    }
    
    private final void init() {
    }
    
    private final void handleLoading(boolean isLoading) {
    }
}