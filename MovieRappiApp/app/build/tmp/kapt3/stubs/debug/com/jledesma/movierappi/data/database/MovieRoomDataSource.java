package com.jledesma.movierappi.data.database;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0011\u0010\u000b\u001a\u00020\fH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ\u0011\u0010\u000e\u001a\u00020\fH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ\u0011\u0010\u000f\u001a\u00020\fH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ!\u0010\u0010\u001a\u0004\u0018\u00010\u00112\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00120\u0007H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0014"}, d2 = {"Lcom/jledesma/movierappi/data/database/MovieRoomDataSource;", "Lcom/jledesma/movierappi/data/datasource/MovieLocalDataSource;", "movieDao", "Lcom/jledesma/movierappi/data/database/MovieDao;", "(Lcom/jledesma/movierappi/data/database/MovieDao;)V", "movies", "Lkotlinx/coroutines/flow/Flow;", "", "Lcom/jledesma/movierappi/domain/Movie;", "getMovies", "()Lkotlinx/coroutines/flow/Flow;", "isEmptyPopular", "", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "isEmptyTrending", "isUpcomingTrending", "save", "Lcom/jledesma/movierappi/domain/Error;", "Lcom/jledesma/movierappi/data/database/DbMovie;", "(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class MovieRoomDataSource implements com.jledesma.movierappi.data.datasource.MovieLocalDataSource {
    private final com.jledesma.movierappi.data.database.MovieDao movieDao = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlinx.coroutines.flow.Flow<java.util.List<com.jledesma.movierappi.domain.Movie>> movies = null;
    
    @javax.inject.Inject()
    public MovieRoomDataSource(@org.jetbrains.annotations.NotNull()
    com.jledesma.movierappi.data.database.MovieDao movieDao) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public kotlinx.coroutines.flow.Flow<java.util.List<com.jledesma.movierappi.domain.Movie>> getMovies() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object isEmptyPopular(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object isEmptyTrending(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object isUpcomingTrending(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.lang.Boolean> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object save(@org.jetbrains.annotations.NotNull()
    java.util.List<com.jledesma.movierappi.data.database.DbMovie> movies, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.jledesma.movierappi.domain.Error> continuation) {
        return null;
    }
}