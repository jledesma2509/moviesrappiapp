package com.jledesma.movierappi;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/jledesma/movierappi/Constants;", "", "()V", "Companion", "app_debug"})
public final class Constants {
    @org.jetbrains.annotations.NotNull()
    public static final com.jledesma.movierappi.Constants.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String FILTER_ORIGINAL_LANGUAGE = "en";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String FILTER_YEAR = "2021";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String POPULAR = "Popular";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TRENDING = "Trending";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UPCOMING = "Upcoming";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String URL_IMAGES = "https://image.tmdb.org/t/p/w500/";
    
    public Constants() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lcom/jledesma/movierappi/Constants$Companion;", "", "()V", "FILTER_ORIGINAL_LANGUAGE", "", "FILTER_YEAR", "POPULAR", "TRENDING", "UPCOMING", "URL_IMAGES", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}