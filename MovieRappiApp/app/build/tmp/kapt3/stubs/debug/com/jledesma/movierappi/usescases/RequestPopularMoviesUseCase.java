package com.jledesma.movierappi.usescases;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0086B\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0007R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\b"}, d2 = {"Lcom/jledesma/movierappi/usescases/RequestPopularMoviesUseCase;", "", "moviesRepository", "Lcom/jledesma/movierappi/data/MoviesRepository;", "(Lcom/jledesma/movierappi/data/MoviesRepository;)V", "invoke", "Lcom/jledesma/movierappi/domain/Error;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class RequestPopularMoviesUseCase {
    private final com.jledesma.movierappi.data.MoviesRepository moviesRepository = null;
    
    @javax.inject.Inject()
    public RequestPopularMoviesUseCase(@org.jetbrains.annotations.NotNull()
    com.jledesma.movierappi.data.MoviesRepository moviesRepository) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object invoke(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.jledesma.movierappi.domain.Error> continuation) {
        return null;
    }
}