package com.jledesma.movierappi.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0013\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000fJ%\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00110\t0\b2\u0006\u0010\u0012\u001a\u00020\u0013H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0015"}, d2 = {"Lcom/jledesma/movierappi/data/MoviesRepository;", "", "localDataSource", "Lcom/jledesma/movierappi/data/datasource/MovieLocalDataSource;", "remoteDataSource", "Lcom/jledesma/movierappi/data/datasource/MovieRemoteDataSource;", "(Lcom/jledesma/movierappi/data/datasource/MovieLocalDataSource;Lcom/jledesma/movierappi/data/datasource/MovieRemoteDataSource;)V", "popularMovies", "Lkotlinx/coroutines/flow/Flow;", "", "Lcom/jledesma/movierappi/domain/Movie;", "getPopularMovies", "()Lkotlinx/coroutines/flow/Flow;", "requestPopularMovies", "Lcom/jledesma/movierappi/domain/Error;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "requestVideosMovies", "Lcom/jledesma/movierappi/domain/Video;", "id", "", "(ILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class MoviesRepository {
    private final com.jledesma.movierappi.data.datasource.MovieLocalDataSource localDataSource = null;
    private final com.jledesma.movierappi.data.datasource.MovieRemoteDataSource remoteDataSource = null;
    @org.jetbrains.annotations.NotNull()
    private final kotlinx.coroutines.flow.Flow<java.util.List<com.jledesma.movierappi.domain.Movie>> popularMovies = null;
    
    @javax.inject.Inject()
    public MoviesRepository(@org.jetbrains.annotations.NotNull()
    com.jledesma.movierappi.data.datasource.MovieLocalDataSource localDataSource, @org.jetbrains.annotations.NotNull()
    com.jledesma.movierappi.data.datasource.MovieRemoteDataSource remoteDataSource) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<java.util.List<com.jledesma.movierappi.domain.Movie>> getPopularMovies() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object requestPopularMovies(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.jledesma.movierappi.domain.Error> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object requestVideosMovies(int id, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlinx.coroutines.flow.Flow<? extends java.util.List<com.jledesma.movierappi.domain.Video>>> continuation) {
        return null;
    }
}