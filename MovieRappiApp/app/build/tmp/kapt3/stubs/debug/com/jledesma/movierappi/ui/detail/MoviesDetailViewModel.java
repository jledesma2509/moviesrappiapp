package com.jledesma.movierappi.ui.detail;

import java.lang.System;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\u0010B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fR\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\t8F\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u0011"}, d2 = {"Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel;", "Landroidx/lifecycle/ViewModel;", "requestVideosMoviesUseCase", "Lcom/jledesma/movierappi/usescases/RequestVideosMoviesUseCase;", "(Lcom/jledesma/movierappi/usescases/RequestVideosMoviesUseCase;)V", "_state", "Lkotlinx/coroutines/channels/Channel;", "Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel$MoviesVideoState;", "state", "Lkotlinx/coroutines/flow/Flow;", "getState", "()Lkotlinx/coroutines/flow/Flow;", "onClicked", "", "id", "", "MoviesVideoState", "app_debug"})
public final class MoviesDetailViewModel extends androidx.lifecycle.ViewModel {
    private final com.jledesma.movierappi.usescases.RequestVideosMoviesUseCase requestVideosMoviesUseCase = null;
    private final kotlinx.coroutines.channels.Channel<com.jledesma.movierappi.ui.detail.MoviesDetailViewModel.MoviesVideoState> _state = null;
    
    @javax.inject.Inject()
    public MoviesDetailViewModel(@org.jetbrains.annotations.NotNull()
    com.jledesma.movierappi.usescases.RequestVideosMoviesUseCase requestVideosMoviesUseCase) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.jledesma.movierappi.ui.detail.MoviesDetailViewModel.MoviesVideoState> getState() {
        return null;
    }
    
    public final void onClicked(int id) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0003\u0004\u0005\u0006B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0004\u0007\b\t\n\u00a8\u0006\u000b"}, d2 = {"Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel$MoviesVideoState;", "", "()V", "Error", "Init", "IsLoading", "SuccessMoviesVideos", "Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel$MoviesVideoState$Init;", "Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel$MoviesVideoState$IsLoading;", "Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel$MoviesVideoState$SuccessMoviesVideos;", "Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel$MoviesVideoState$Error;", "app_debug"})
    public static abstract class MoviesVideoState {
        
        private MoviesVideoState() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel$MoviesVideoState$Init;", "Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel$MoviesVideoState;", "()V", "app_debug"})
        public static final class Init extends com.jledesma.movierappi.ui.detail.MoviesDetailViewModel.MoviesVideoState {
            @org.jetbrains.annotations.NotNull()
            public static final com.jledesma.movierappi.ui.detail.MoviesDetailViewModel.MoviesVideoState.Init INSTANCE = null;
            
            private Init() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0006\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0007\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\b\u001a\u00020\u00032\b\u0010\t\u001a\u0004\u0018\u00010\nH\u00d6\u0003J\t\u0010\u000b\u001a\u00020\fH\u00d6\u0001J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0005\u00a8\u0006\u000f"}, d2 = {"Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel$MoviesVideoState$IsLoading;", "Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel$MoviesVideoState;", "isLoading", "", "(Z)V", "()Z", "component1", "copy", "equals", "other", "", "hashCode", "", "toString", "", "app_debug"})
        public static final class IsLoading extends com.jledesma.movierappi.ui.detail.MoviesDetailViewModel.MoviesVideoState {
            private final boolean isLoading = false;
            
            @org.jetbrains.annotations.NotNull()
            public final com.jledesma.movierappi.ui.detail.MoviesDetailViewModel.MoviesVideoState.IsLoading copy(boolean isLoading) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public IsLoading(boolean isLoading) {
                super();
            }
            
            public final boolean component1() {
                return false;
            }
            
            public final boolean isLoading() {
                return false;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0012"}, d2 = {"Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel$MoviesVideoState$SuccessMoviesVideos;", "Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel$MoviesVideoState;", "videos", "", "Lcom/jledesma/movierappi/domain/Video;", "(Ljava/util/List;)V", "getVideos", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "app_debug"})
        public static final class SuccessMoviesVideos extends com.jledesma.movierappi.ui.detail.MoviesDetailViewModel.MoviesVideoState {
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<com.jledesma.movierappi.domain.Video> videos = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.jledesma.movierappi.ui.detail.MoviesDetailViewModel.MoviesVideoState.SuccessMoviesVideos copy(@org.jetbrains.annotations.NotNull()
            java.util.List<com.jledesma.movierappi.domain.Video> videos) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public SuccessMoviesVideos(@org.jetbrains.annotations.NotNull()
            java.util.List<com.jledesma.movierappi.domain.Video> videos) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<com.jledesma.movierappi.domain.Video> component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<com.jledesma.movierappi.domain.Video> getVideos() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel$MoviesVideoState$Error;", "Lcom/jledesma/movierappi/ui/detail/MoviesDetailViewModel$MoviesVideoState;", "rawResponse", "", "(Ljava/lang/String;)V", "getRawResponse", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "app_debug"})
        public static final class Error extends com.jledesma.movierappi.ui.detail.MoviesDetailViewModel.MoviesVideoState {
            @org.jetbrains.annotations.NotNull()
            private final java.lang.String rawResponse = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.jledesma.movierappi.ui.detail.MoviesDetailViewModel.MoviesVideoState.Error copy(@org.jetbrains.annotations.NotNull()
            java.lang.String rawResponse) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Error(@org.jetbrains.annotations.NotNull()
            java.lang.String rawResponse) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String getRawResponse() {
                return null;
            }
        }
    }
}