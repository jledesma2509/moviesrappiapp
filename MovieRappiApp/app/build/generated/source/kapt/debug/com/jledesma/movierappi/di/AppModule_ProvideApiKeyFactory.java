// Generated by Dagger (https://dagger.dev).
package com.jledesma.movierappi.di;

import android.app.Application;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import dagger.internal.QualifierMetadata;
import dagger.internal.ScopeMetadata;
import javax.inject.Provider;

@ScopeMetadata("javax.inject.Singleton")
@QualifierMetadata("com.jledesma.movierappi.di.ApiKey")
@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppModule_ProvideApiKeyFactory implements Factory<String> {
  private final Provider<Application> appProvider;

  public AppModule_ProvideApiKeyFactory(Provider<Application> appProvider) {
    this.appProvider = appProvider;
  }

  @Override
  public String get() {
    return provideApiKey(appProvider.get());
  }

  public static AppModule_ProvideApiKeyFactory create(Provider<Application> appProvider) {
    return new AppModule_ProvideApiKeyFactory(appProvider);
  }

  public static String provideApiKey(Application app) {
    return Preconditions.checkNotNullFromProvides(AppModule.INSTANCE.provideApiKey(app));
  }
}
