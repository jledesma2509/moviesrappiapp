/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.jledesma.movierappi;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.jledesma.movierappi";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Field from default config.
  public static final String API_URL_BASE = "https://api.themoviedb.org/3/";
  // Field from default config.
  public static final String YOUTUBE_KEY = "AIzaSyBmtZBJJfS-5dzDqxBuWhOz23-qliWbKC0";
}
